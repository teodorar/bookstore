import React, {useState} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useDispatch, useSelector} from 'react-redux'
import FormContainer from '../components/FormContainer'
import CheckoutSteps from '../components/CheckoutSteps'
import { FormLabel, FormControl } from 'react-bootstrap'
import { saveShippingAddress } from '../actions/cartActions'

const ShippingScreen = ({history}) => {

    const cart = useSelector(state => state.cart)
    const {shippingAddress} = cart

    const[address, setAddress] = useState(shippingAddress.address)
    const[city, setCity] = useState(shippingAddress.city)
    const[postalCode, setPostalCode] = useState(shippingAddress.postalCode)
    const[country, setCountry] = useState(shippingAddress.country)

    const dispatch = useDispatch()

    const submitHandler = (e) =>{
        e.preventDefault()
        dispatch(saveShippingAddress({address, city, postalCode, country}))
        history.push('payment')
    }


    return (
        <FormContainer>
            <CheckoutSteps step1 step2 />
            <h1>Shipping</h1>
            <Form onSubmit={submitHandler}>
            <Form.Group controlId='address'>
                <FormLabel>Address</FormLabel>
                <FormControl type='text' placeholder='enter your address' value={address} onChange={(e) => setAddress(e.target.value)}></FormControl>
            </Form.Group>
            <Form.Group controlId='city'>
                <FormLabel>City</FormLabel>
                <FormControl type='text' placeholder='enter your city' value={city} onChange={(e) => setCity(e.target.value)}></FormControl>
            </Form.Group>
            <Form.Group controlId='postalCode'>
                <FormLabel>Postal Code</FormLabel>
                <FormControl type='text' placeholder='enter your postal code' value={postalCode} onChange={(e) => setPostalCode(e.target.value)}></FormControl>
            </Form.Group>
            <Form.Group controlId='country'>
                <FormLabel>Country</FormLabel>
                <FormControl type='text' placeholder='enter your country' value={country} onChange={(e) => setCountry(e.target.value)}></FormControl>
            </Form.Group>
            <Button type='submit' variant='primary'>Continue</Button>
            </Form>
        </FormContainer>
    )
}

export default ShippingScreen
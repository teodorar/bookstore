import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import FormContainer from '../components/FormContainer'
import { listProductDetails, updateProduct } from '../actions/productActions'
import { PRODUCT_UPDATE_RESET } from '../constants/productConstants'

const ProductEditScreen = ({ match, history }) => {
  const productId = match.params.id

  const [name, setName] = useState('')
  const [price, setPrice] = useState(0)
  const [author, setAuthor] = useState('')
  const [image, setImage] = useState('')
  const [format, setFormat] = useState('')
  const [category, setCategory] = useState('')
  const [countInStock, setCountInStock] = useState(0)
  const [description, setDescription] = useState('')
  const [stateOfProduct, setStateOfProduct] = useState('')
  const [pageNumber, setPageNumber] = useState(0)
  const [uploading, setUploading] = useState(false)

  const dispatch = useDispatch()

  const productDetails = useSelector((state) => state.productDetails)
  const { loading, error, product } = productDetails

  const productUpdate = useSelector((state) => state.productUpdate)
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = productUpdate

  useEffect(() => {
    if (successUpdate) {
      dispatch({ type: PRODUCT_UPDATE_RESET })
      history.push('/admin/productlist')
    } else {
      if (!product.name || product._id !== productId) {
        dispatch(listProductDetails(productId))
      } else {
        setName(product.name)
        setPrice(product.price)
        setAuthor(product.author)
        setImage(product.image)
        setFormat(product.format)
        setCategory(product.category)
        setCountInStock(product.countInStock)
        setDescription(product.description)
        setStateOfProduct(product.stateOfProduct)
        setPageNumber(product.pageNumber)
      }
    }
  }, [dispatch, history, productId, product, successUpdate])

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0]
    const formData = new FormData()
    formData.append('image', file)
    setUploading(true)

    try {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }

      const { data } = await axios.post('/api/upload', formData, config)

      setImage(data)
      setUploading(false)
    } catch (error) {
      console.error(error)
      setUploading(false)
    }
  }

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(
      updateProduct({
        _id: productId,
        name,
        author,
        price,
        image,
        format,
        category,
        description,
        countInStock,
        stateOfProduct,
        pageNumber,
      })
    )
  }

  return (
    <>
      <Link to='/admin/productlist' className='btn btn-light my-3'>
        Go Back
      </Link>
      <FormContainer>
        <h1>Edit Product</h1>
        {loadingUpdate && <Loader />}
        {errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
        {loading ? (
          <Loader />
        ) : error ? (
          <Message variant='danger'>{error}</Message>
        ) : (
          <Form onSubmit={submitHandler}>
            <Form.Group controlId='name'>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type='name'
                placeholder='Enter name'
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='price'>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type='number'
                placeholder='Enter price'
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='author'>
              <Form.Label>Author</Form.Label>
              <Form.Control
                as='select'
                value={author}
                onChange={(e) => setAuthor(e.target.value)}
                >
                <option value=''>Select Author gender</option>
                <option value='1'>Female</option>
                <option value='2'>Male</option>
                <option value='3'>Other</option>
                </Form.Control>
             </Form.Group>

            <Form.Group controlId='image'>
              <Form.Label>Image</Form.Label>
              <Form.Control
                type='text'
                placeholder='Enter image url'
                value={image}
                onChange={(e) => setImage(e.target.value)}
              ></Form.Control>
              <Form.File
                id='image-file'
                label='Choose File'
                custom
                onChange={uploadFileHandler}
              ></Form.File>
              {uploading && <Loader />}
            </Form.Group>

            <Form.Group controlId='format'>
              <Form.Label>Format</Form.Label>
              <Form.Control
                as='select'
                value={format}
                onChange={(e) => setFormat(e.target.value)}
                >
                <option value=''>Select Format</option>
                <option value='Hardback'>Hardback</option>
                <option value='PaperBack'>PaperBack</option>
                </Form.Control>
             </Form.Group>

            <Form.Group controlId='countInStock'>
              <Form.Label>Books in stock</Form.Label>
              <Form.Control
                type='number'
                placeholder='Enter books in stock'
                value={countInStock}
                onChange={(e) => setCountInStock(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='category'>
              <Form.Label>Category</Form.Label>
              <Form.Control
                as='select'
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                >
                <option value=''>Select Category</option>
                <option value='Fantasy'>Fantasy</option>
                <option value='Adventure'>Adventure</option>
                <option value='Romance'>Romance</option>
                <option value='Contemporary'>Contemporary</option>
                <option value='Dystopian'>Dystopian</option>
                <option value='Mystery'>Mystery</option>
                <option value='Horror'>Horror</option>
                <option value='Thriller'>Thriller</option>
                <option value='Paranormal'>Paranormal</option>
                <option value='Historical fiction'>Historical fiction</option>
                <option value='Science fiction'>Science fiction</option>
                <option value='Memoir'>Memoir</option>
                <option value='Cooking'>Cooking</option>
                <option value='Art'>Art</option>
                <option value='Self-help'>Self-help</option>
                <option value='Development'>Development</option>
                <option value='Motivational'>Motivational</option>
                <option value='Health'>Health</option>
                <option value='History'>History</option>
                <option value='Travel'>Travel</option>
                <option value='Guide/How-to'>Guide/How-to</option>
                <option value='Families and Relationships'>Families and Relationships</option>
                <option value='Humor'>Humor</option>
                <option value='Childrens'>Children's</option>
                </Form.Control>
             </Form.Group>

            <Form.Group controlId='description'>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type='text'
                placeholder='Enter description'
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='stateOfProduct'>
              <Form.Label>State of book</Form.Label>
              <Form.Control
                as='select'
                value={stateOfProduct}
                onChange={(e) => setStateOfProduct(e.target.value)}
                >
                <option value=''>Select state of the book</option>
                <option value='Barely Used / Like new'>Barely Used / Like new</option>
                <option value='Used'>Used</option>
                <option value='Heavily used'>Heavily used</option>
                <option value='Annotated'>Annotated</option>
                </Form.Control>
             </Form.Group>

            <Form.Group controlId='pageNumber'>
              <Form.Label>Page number</Form.Label>
              <Form.Control
                type='number'
                placeholder='Enter number of pages'
                value={pageNumber}
                onChange={(e) => setPageNumber(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Button type='submit' variant='primary'>
              Update
            </Button>
          </Form>
        )}
      </FormContainer>
    </>
  )
}

export default ProductEditScreen
import bcrypt from 'bcryptjs'

const users = [
    {
        name: 'Admin User',
        email: 'admin@example.com',
        password: bcrypt.hashSync('12369', 10),
        isAdmin: true
    },
    {
        name: 'Marko Stankov',
        email: 'marko@example.com',
        password: bcrypt.hashSync('12369', 10)
    },
    {
        name: 'Teodora Ristic',
        email: 'teodora@example.com',
        password: bcrypt.hashSync('12369', 10)
    },
]

export default users